<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@utama');
Route::get('/biodata', 'AuthController@bio');
Route::post('/kirim', 'AuthController@kirim');
Route::get('/data-table', 'IndexController@tabel');

//CRUD Cast

//Create Data
//menuju ke form tambah cast
Route::get('/cast/create', 'CastController@create');
//untuk mengirim inputan ke database
Route::post('/cast', 'CastController@store');

//Read Data
//untuk menampilkan semua database di web browser
Route::get('/cast', 'CastController@index');
//detail data berdasarkan id
Route::get('/cast/{cast_id}', 'CastController@show');

//Update data
//menuju ke form edit kategori berdasarkan id
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//untuk update berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete Data
Route::delete('/cast/{cast_id}', 'CastController@destroy');



//CRUD Film
Route::resource('film', 'FilmController');

Auth::routes();


Route::middleware(['auth'])->group(function () {
    //CRUD Genre

//Create Data
//menuju ke form tambah genre
Route::get('/genre/create', 'GenreController@create');
//untuk mengirim inputan ke database
Route::post('/genre', 'GenreController@store');

//Read Data
//untuk menampilkan semua database di web browser
Route::get('/genre', 'GenreController@index');
//detail data berdasarkan id
Route::get('/genre/{genre_id}', 'GenreController@show');

//Update data
//menuju ke form edit genre berdasarkan id
Route::get('/genre/{genre_id}/edit', 'GenreController@edit');
//untuk update berdasarkan id
Route::put('/genre/{genre_id}', 'GenreController@update');

//Delete Data
Route::delete('/genre/{genre_id}', 'GenreController@destroy');
});

//Tambah komentar
Route::post('/kritik', 'KritikController@store');