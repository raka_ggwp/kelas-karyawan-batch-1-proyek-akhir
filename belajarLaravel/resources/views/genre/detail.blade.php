@extends('layout.master')
@section('judul')
Halaman Detail Cast
@endsection

@section('content')

<h1>{{$genreDetail->nama}}</h1>


<div class="row">

    @forelse ($genreDetail->film as $item)
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{asset('image/'.$item->poster)}}" style="width: 40vh; height:400px" alt="Card image cap">
                <div class="card-body">
                  <h2>{{$item->judul}}</h2>
                  <p class="card-text">{{ Str::limit($item->ringkasan, 40) }}</p>
                  <a href="/film/{{$item->id}}" class="btn btn-primary">Read More</a>
                </div>
            </div>
        </div>
    @empty
        <h2>Tidak Ada Film di Genre ini</h2>
    @endforelse
</div>



<a href="/genre" class="btn btn-secondary btn-sm">Kembali</a>
@endsection