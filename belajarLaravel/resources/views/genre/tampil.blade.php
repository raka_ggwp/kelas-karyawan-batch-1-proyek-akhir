@extends('layout.master')
@section('judul')
Halaman List Genre
@endsection

@section('content')
<a href="/genre/create" class="btn btn-primary btn-sm">Tambah Genre</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/genre/{{$item->id}}" method="POST">
                      <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                      <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                      @csrf
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data Genre Kosong</h1>
        @endforelse
    </tbody>
  </table>
@endsection