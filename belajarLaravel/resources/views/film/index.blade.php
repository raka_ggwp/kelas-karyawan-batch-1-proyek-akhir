@extends('layout.master')
@section('judul')
Halaman List Film
@endsection

@section('content')

@auth
<a href="/film/create" class="btn btn-primary btn-sm my-3">Tambah Film</a>
@endauth

<div class="row">
    @forelse ($film as $item)
        <div class="col-3">
            <div class="card" style="height: 600px">
                <img class="card-img-top" src="{{asset('image/'. $item->poster)}}" style="height: 400px" alt="...">
                <div class="card-body">
                  <h2>{{$item->judul}}</h2>
                  <span class="badge badge-primary">{{$item->genre->nama}}</span>
                  <p class="card-text">{{ Str::limit($item->ringkasan, 40) }}</p>
                  @auth
                  <form action="/film/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  </form>
                  @endauth

                  @guest
                  <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  @endguest
                </div>
              </div>
        </div>
    @empty
        <h1>Tidak Ada Film</h1>
    @endforelse
    
</div>

@endsection