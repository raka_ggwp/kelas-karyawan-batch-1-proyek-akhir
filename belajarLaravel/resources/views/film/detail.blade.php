@extends('layout.master')
@section('judul')
Halaman Detail Film
@endsection

@section('content')

            
                <img class="card-img-top" src="{{asset('image/'. $film->poster)}}" style="width: 40vh; height:400px" alt="Card image cap">
                
                  <h2>{{$film->judul}}</h2>
                  <p class="card-text">{{$film->ringkasan, 60}}</p>
                  <a href="/film" class="btn btn-success">Kembali</a>
                
        <hr>

        {{-- List Komentar --}}

        <h1>List Komentar</h1>

        @forelse ($film->kritik as $item)
        <div class="card">
          <div class="card-header">
            {{$item->user->name}}
          </div>
          <div class="card-body">
            <h2 class="text-warning">Point {{$item->point}}/5</h2>
            <p class="card-text">{{$item->content}}</p>
          </div>
        </div>
        @empty
            <h2>Belum Ada Komentar</h2>
        @endforelse
        
        <hr>
        
{{-- Form Tambah Film --}}
  <form action="/kritik" method="POST" class="mt-4">
    @csrf
    <div class="form-group">
      <select name="point" class="form-control" id="">
        <option value="">--Pilih Point yang diberikan--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select>
    </div>
    @error('point')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <input type="hidden" value="{{$film->id}}" name="film_id">
      <textarea name="content" rows="10" cols="30" placeholder="Isi Komentar" class="form-control">{{old('content')}}</textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <input type="submit" class="btn btn-primary btn-sm" value="Post Komentar">
  </form>

@endsection