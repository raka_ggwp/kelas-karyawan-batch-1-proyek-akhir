<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;

class GenreController extends Controller
{
    public function create()
    {
        return view('genre.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            
        ]);

        $genre = new Genre;
        $genre->nama = $request->nama;
       
        $genre->save();

        return redirect('/genre');
    }

    public function index()
    {
        $genre = Genre::all();
        
        return view('genre.tampil', compact('genre'));
    }

    public function show($id)
    {
        $genreDetail = Genre::find($id);
        return view('genre.detail', compact('genreDetail'));
    }

    public function edit($id)
    {
        $genreDetail = Genre::find($id);
        return view('genre.edit', compact('genreDetail'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            
        ]);

        $genre = Genre::find($id);

        $genre->nama = $request->nama;
        

        $genre->save();

        return redirect('/genre');
    }

    public function destroy($id)
    {
        $genre = Genre::find($id);
        $genre->delete();

        return redirect('/genre');
    }
}
